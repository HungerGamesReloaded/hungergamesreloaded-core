package nl.spiderdead.hungergamesreloadedcore.Commands;

import nl.spiderdead.hungergamesreloadedcore.Infra.Services.HGRCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class HungerGamesReloaded extends HGRCommand {
    public HungerGamesReloaded() {
        this.setName("hungergamesreloaded");
        this.setPermission("hugergamesreloaded.hgr");
        this.setPlayerCommand(true);
    }

    @Override
    public boolean command(CommandSender sender, Command cmd, String label, String[] args) {
        sendMessage("&a» &6HungerGames&eReloaded &a«");
        sendMessage("");
        sendMessage("&6- /hgr &8- &eThis exact menu! POGGERS");
        sendMessage("&6- /hgr setup &8- &eUse this to setup the current world.");
        sendMessage("&6- /hgr start &8- &eStart the game without waiting for more players.");
        sendMessage("&6- /hgr stop &8- &eThis restarts the server and kicks everyone to the lobby.");
        sendMessage("");
        return false;
    }
}
