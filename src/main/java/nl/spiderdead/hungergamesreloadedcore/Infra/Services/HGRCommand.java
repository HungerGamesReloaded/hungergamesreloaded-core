package nl.spiderdead.hungergamesreloadedcore.Infra.Services;

import org.jetbrains.annotations.NotNull;
import nl.spiderdead.hungergamesreloadedcore.Infra.Services.Helpers.Text;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public abstract class HGRCommand implements CommandExecutor {
    private String name;
    private CommandSender sender;
    protected Player player;
    private int defaultArgs = 0;
    private String permission;
    private boolean playerCommand = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    @Deprecated
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {
        setSender(sender);

        if (this.permission != null && !sender.hasPermission(permission)) {
            this.noPerms();
            return true;
        }

        if (this.defaultArgs > 0 && args.length == 0) {
            this.noArgs();
            return true;
        }

        if (this.playerCommand && sender instanceof ConsoleCommandSender) {
            sendMessage("&cDit is een speler commando");
            return false;
        }

        return this.command(sender, command, label, args);
    }

    public void noArgs() {
    }

    abstract public boolean command(CommandSender sender, Command cmd, String label, String[] args);

    protected void header(String subject) {
        sendMessage("");
        sendMessage("&6&lHungerGames Reloaded &8» " + subject);
        sendMessage("");
    }

    protected void sendMessage(String s) {
        this.sender.sendMessage(Text.colorize(s));
    }

    protected void sendMessage(Player p, String s) {
        p.sendMessage(Text.colorize(s));
    }

    protected boolean hasPermission(String s) {
        return this.player.hasPermission(s);
    }

    protected boolean checkPermission(String s) {
        if (sender instanceof ConsoleCommandSender) return true;
        if (!this.player.hasPermission(s)) {
            this.noPerms();
            return false;
        }
        return true;
    }

    protected void noPerms() {
        sendMessage("&cJe hebt geen toegang tot dit commando");
    }

    protected Player getPlayer() {
        return player;
    }

    protected boolean isPlayer() {
        return this.sender instanceof Player;
    }

    protected boolean isPlayer(String playerName) {
        return Bukkit.getPlayer(playerName) != null;
    }

    public void setSender(CommandSender sender) {
        this.sender = sender;
        if (this.isPlayer()) {
            this.player = (Player) this.sender;
        }
    }

    public int getDefaultArgs() {
        return defaultArgs;
    }

    public void setDefaultArgs(int defaultArgs) {
        this.defaultArgs = defaultArgs;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public void setPlayerCommand(boolean playerCommand) {
        this.playerCommand = playerCommand;
    }
}
