package nl.spiderdead.hungergamesreloadedcore.Infra.Services.Registers;

import nl.spiderdead.hungergamesreloadedcore.Commands.HungerGamesReloaded;
import nl.spiderdead.hungergamesreloadedcore.Infra.Services.HGRCommand;
import nl.spiderdead.hungergamesreloadedcore.Main;
import org.bukkit.command.PluginCommand;

public class CommandRegisterer {
    public static void register() {
        registerCommand(new HungerGamesReloaded());
    }

    private static void registerCommand(HGRCommand command) {
        PluginCommand bukkitCommand = Main.instance.getCommand(command.getName());
        if (bukkitCommand != null) {
            bukkitCommand.setExecutor(command);
        }
    }
}
