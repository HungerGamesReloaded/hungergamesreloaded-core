package nl.spiderdead.hungergamesreloadedcore;

import nl.spiderdead.hungergamesreloadedcore.Data.Models.Game;
import nl.spiderdead.hungergamesreloadedcore.Infra.Services.Registers.CommandRegisterer;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class Main extends JavaPlugin {
    public static Main instance;
    public static Game currentGame;

    @Override
    public void onEnable() {
        instance = this;
        CommandRegisterer.register();
        Bukkit.getLogger().info("[HungerGamesReloaded] Core has been started!");
        currentGame = new Game();
        currentGame.start();

    }

    @Override
    public void onDisable() {
        Bukkit.getLogger().info("[HungerGamesReloaded] Core has been terminated! BOOM!");
    }
}
